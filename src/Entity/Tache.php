<?php

namespace App\Entity;


use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\TacheRepository")
 */
class Tache
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $titre;

    /**
     * @ORM\Column(type="text")
     */
    private $description;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $etat;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $priorite;

    /**
     * @ORM\Column(type="float")
     */
    private $tauxAvancement;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $positionProjet;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $positionUtilisateur;
    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Projet",inversedBy="taches")
     */
    private $projet;
    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User",inversedBy="taches")
     */
    private $utilisateur;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitre(): ?string
    {
        return $this->titre;
    }

    public function setTitre(string $titre): self
    {
        $this->titre = $titre;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getEtat(): ?string
    {
        return $this->etat;
    }

    public function setEtat(string $etat): self
    {
        $this->etat = $etat;

        return $this;
    }

    public function getPriorite(): ?string
    {
        return $this->priorite;
    }

    public function setPriorite(?string $priorite): self
    {
        $this->priorite = $priorite;

        return $this;
    }

    public function getTauxAvancement(): ?float
    {
        return $this->tauxAvancement;
    }

    public function setTauxAvancement(float $tauxAvancement): self
    {
        $this->tauxAvancement = $tauxAvancement;

        return $this;
    }

    public function getPositionProjet(): ?int
    {
        return $this->positionProjet;
    }

    public function setPositionProjet(?int $positionProjet): self
    {
        $this->positionProjet = $positionProjet;

        return $this;
    }

    public function getPositionUtilisateur(): ?int
    {
        return $this->positionUtilisateur;
    }

    public function setPositionUtilisateur(?int $positionUtilisateur): self
    {
        $this->positionUtilisateur = $positionUtilisateur;

        return $this;
    }

    /**
     * Get the value of projet
     */ 
    public function getProjet(): ?Projet
    {
        return $this->projet;
    }

    /**
     * Set the value of projet
     *
     * @return  self
     */ 
    public function setProjet($projet)
    {
        $this->projet = $projet;

        return $this;
    }

    /**
     * Get the value of utilisateur
     */ 
    public function getUtilisateur(): ?Utilisateur
    {
        return $this->utilisateur;
    }

    /**
     * Set the value of utilisateur
     *
     * @return  self
     */ 
    public function setUtilisateur($utilisateur)
    {
        $this->utilisateur = $utilisateur;

        return $this;
    }
}
